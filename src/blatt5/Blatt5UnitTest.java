package blatt5;

import java.util.HashSet;

public class Blatt5UnitTest {

    /* single state with loop
     * DFA
     * Alphabet: a
     * States: s0
     * Init: s0
     * Final: s0
     * Transitions:
     * s0;a;s0
     * END
     *
     * Answer: false
     */
    private static void runTestA_1() {
        State s0 = new State();
        HashSet<State> states = new HashSet<>();
        states.add(s0);

        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');

        HashSet<Transition> transitions = new HashSet<>();
        transitions.add(new Transition(s0, s0, 'a'));

        DFA d = new DFA(states, transitions, alphabet, s0, states);
        assert !IsFinite.isFinite(d);
    }

    /* has a loop but there are no final states reachable from it
     * DFA
     * Alphabet: a
     * States: s0; s1; s2
     * Init: s0
     * Final: s0
     * Transitions:
     * s0;a;s1
     * s1;a;s2
     * s2;a;s1
     * END
     *
     * Answer: true
     */
    private static void runTestA_2() {
        State s0 = new State();
        State s1 = new State();
        State s2 = new State();
        HashSet<State> states = new HashSet<>();
        states.add(s0);
        states.add(s1);
        states.add(s2);

        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');

        HashSet<Transition> transitions = new HashSet<>();
        transitions.add(new Transition(s0, s1, 'a'));
        transitions.add(new Transition(s1, s2, 'a'));
        transitions.add(new Transition(s2, s1, 'a'));

        HashSet<State> finalStates = new HashSet<>();
        finalStates.add(s0);

        DFA d = new DFA(states, transitions, alphabet, s0, finalStates);
        assert IsFinite.isFinite(d);
    }

    /*
     * DFA
     * Alphabet: a;b
     * States: p;q;r
     * Init: p
     * Final: q
     * Transitions:
     * p;a;p
     * p;b;q
     * q;a;r
     * q;b;r
     * r;a;r
     * r;b;r
     * END
     *
     * Answer: false
     */
    private static void runTestA_3() {
        State p = new State();
        State q = new State();
        State r = new State();
        HashSet<State> states = new HashSet<>();
        states.add(p);
        states.add(q);
        states.add(r);

        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');

        HashSet<Transition> transitions = new HashSet<>();
        transitions.add(new Transition(p, p, 'a'));
        transitions.add(new Transition(p, q, 'b'));
        transitions.add(new Transition(q, r, 'a'));
        transitions.add(new Transition(q, r, 'b'));
        transitions.add(new Transition(r, r, 'a'));
        transitions.add(new Transition(r, r, 'b'));

        HashSet<State> finalStates = new HashSet<>();
        finalStates.add(q);

        DFA d = new DFA(states, transitions, alphabet, p, finalStates);
        assert !IsFinite.isFinite(d);
    }


    public static void runTestsA() {
        System.out.print("Running tests for Task A...");
        runTestA_1();
        runTestA_2();
        runTestA_3();
        System.out.println("Done");
    }

    /* test for an unreachable end state
     * DFA
     * Alphabet: a
     * States: s0; s1
     * Init: s0
     * Final: s1
     * Transitions:
     * s0;a;s0
     * s1;a;s1
     * END
     *
     * Answer: true
     */
    public static void runTestB_1() {
        State s0 = new State();
        State s1 = new State();
        HashSet<State> states = new HashSet<>();
        states.add(s0);
        states.add(s1);

        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');

        HashSet<Transition> transitions = new HashSet<>();
        transitions.add(new Transition(s0, s0, 'a'));
        transitions.add(new Transition(s1, s1, 'a'));

        HashSet<State> finalStates = new HashSet<>();
        finalStates.add(s1);

        DFA d = new DFA(states, transitions, alphabet, s0, finalStates);
        assert IsEmpty.isEmpty(d);
    }

    /*
     * DFA
     * Alphabet: a;b
     * States: p;q;r
     * Init: p
     * Final: r
     * Transitions:
     * p;a;q
     * p;b;q
     * q;a;p
     * q;b;p
     * r;a;r
     * r;b;q
     * END
     *
     * Answer: true
     */
    public static void runTestB_2() {
        State p = new State();
        State q = new State();
        State r = new State();
        HashSet<State> states = new HashSet<>();
        states.add(p);
        states.add(q);
        states.add(r);

        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');

        HashSet<Transition> transitions = new HashSet<>();
        transitions.add(new Transition(p, q, 'a'));
        transitions.add(new Transition(p, q, 'b'));
        transitions.add(new Transition(q, p, 'a'));
        transitions.add(new Transition(q, p, 'b'));
        transitions.add(new Transition(r, r, 'a'));
        transitions.add(new Transition(r, q, 'b'));

        HashSet<State> finalStates = new HashSet<>();
        finalStates.add(r);

        DFA d = new DFA(states, transitions, alphabet, p, finalStates);
        assert IsEmpty.isEmpty(d);
    }

    public static void runTestsB() {
        System.out.print("Running tests for Task B...");
        runTestB_1();
        runTestB_2();
        System.out.println("Done");
    }

    public static void main(String[] args) {
        runTestsA();
        runTestsB();
    }
}
