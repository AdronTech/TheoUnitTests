package blatt9;

import java.util.HashSet;

public class Blatt9UnitTest {
    private static PDA get01PDA() {
        State p = new State("p");
        State q = new State("q");
        HashSet<State> states = new HashSet<>();
        states.add(p);
        states.add(q);

        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('0');
        alphabet.add('1');
        HashSet<Character> stack = new HashSet<>(alphabet);
        stack.add('F');

        HashSet<PDATransition> transitions = new HashSet<>();
        transitions.add(new PDATransition(p, Util.EPSILON, 'F', q, "011110F"));
        transitions.add(new PDATransition(q, '0', '0', q, ""));
        transitions.add(new PDATransition(q, '1', '1', q, ""));
        transitions.add(new PDATransition(q, Util.EPSILON, 'F', q, ""));

        return new PDA(states, transitions, alphabet, p, stack, 'F');
    }

    private static void runTestA_1() {
        PDA m = get01PDA();

        assert !AcceptPDA.accept(m, "");
        assert AcceptPDA.accept(m, "011110");
        assert !AcceptPDA.accept(m, "011110111");
        assert !AcceptPDA.accept(m, "011110000");
        assert !AcceptPDA.accept(m, "1111110");
        assert !AcceptPDA.accept(m, "111110000");
        assert !AcceptPDA.accept(m, "111110111");
    }

    private static PDA get01PrefixPDA() {
        State __p = new State("__p");
        State __q = new State("__q");
        State p = new State("p");
        State q = new State("q");
        HashSet<State> states = new HashSet<>();
        states.add(__p);
        states.add(p);
        states.add(__q);
        states.add(q);

        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('0');
        alphabet.add('1');
        HashSet<Character> stack = new HashSet<>(alphabet);
        stack.add('F');

        HashSet<PDATransition> transitions = new HashSet<>();

        transitions.add(new PDATransition(p, Util.EPSILON, 'F', q, "011110F"));
        transitions.add(new PDATransition(q, '0', '0', q, ""));
        transitions.add(new PDATransition(q, '1', '1', q, ""));
        transitions.add(new PDATransition(q, Util.EPSILON, 'F', q, ""));

        transitions.add(new PDATransition(__p, Util.EPSILON, 'F', __q, "011110F"));
        transitions.add(new PDATransition(__q, Util.EPSILON, '0', __q, ""));
        transitions.add(new PDATransition(__q, Util.EPSILON, '1', __q, ""));
        transitions.add(new PDATransition(__q, Util.EPSILON, 'F', __q, ""));

        transitions.add(new PDATransition(p, Util.EPSILON, '0', __p, "0"));
        transitions.add(new PDATransition(p, Util.EPSILON, '1', __p, "1"));
        transitions.add(new PDATransition(p, Util.EPSILON, 'F', __p, "F"));
        transitions.add(new PDATransition(q, Util.EPSILON, '0', __q, "0"));
        transitions.add(new PDATransition(q, Util.EPSILON, '1', __q, "1"));
        transitions.add(new PDATransition(q, Util.EPSILON, 'F', __q, "F"));

        return new PDA(states, transitions, alphabet, p, stack, 'F');
    }

    private static void check01Prefix(PDA m) {
        assert AcceptPDA.accept(m, "");
        assert AcceptPDA.accept(m, "011110");
        assert !AcceptPDA.accept(m, "011110111");
        assert !AcceptPDA.accept(m, "011110000");
        assert !AcceptPDA.accept(m, "1111110");
        assert AcceptPDA.accept(m, "0");
        assert AcceptPDA.accept(m, "011");
        assert !AcceptPDA.accept(m, "1");
        assert AcceptPDA.accept(m, "01111");
        assert AcceptPDA.accept(m, "01");
    }

    private static void runTestA_2() {
        check01Prefix(get01PrefixPDA());
    }

    private static PDA getAnBnPDA() {
        State p = new State("p");
        State q = new State("q");
        HashSet<State> states = new HashSet<>();
        states.add(p);
        states.add(q);

        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        HashSet<Character> stack = new HashSet<>();
        stack.add('A');
        stack.add('X');

        HashSet<PDATransition> transitions = new HashSet<>();
        transitions.add(new PDATransition(p, 'b', 'A', p, ""));
        transitions.add(new PDATransition(q, 'a', 'A', q, "AA"));
        transitions.add(new PDATransition(q, 'a', 'X', q, "A"));
        transitions.add(new PDATransition(q, 'b', 'A', p, ""));

        return new PDA(states, transitions, alphabet, q, stack, 'X');
    }

    private static void runTestA_3() {
        PDA m = getAnBnPDA();

        assert !AcceptPDA.accept(m, "");
        assert AcceptPDA.accept(m, "ab");
        assert !AcceptPDA.accept(m, "aab");
        assert !AcceptPDA.accept(m, "abb");
        assert AcceptPDA.accept(m, "aabb");
        assert !AcceptPDA.accept(m, "aaabb");
        assert !AcceptPDA.accept(m, "aababb");
    }

    private static PDA getAnBnPrefix() {
        State p = new State("p");
        State __p = new State("__p");
        State q = new State("q");
        State __q = new State("__q");
        HashSet<State> states = new HashSet<>();
        states.add(p);
        states.add(q);
        states.add(__p);
        states.add(__q);

        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        HashSet<Character> stack = new HashSet<>();
        stack.add('A');
        stack.add('X');

        HashSet<PDATransition> transitions = new HashSet<>();
        transitions.add(new PDATransition(p, 'b', 'A', p, ""));
        transitions.add(new PDATransition(q, 'a', 'A', q, "AA"));
        transitions.add(new PDATransition(q, 'a', 'X', q, "A"));
        transitions.add(new PDATransition(q, 'b', 'A', p, ""));

        transitions.add(new PDATransition(__p, Util.EPSILON, 'A', __p, ""));
        transitions.add(new PDATransition(__q, Util.EPSILON, 'A', __q, "AA"));
        transitions.add(new PDATransition(__q, Util.EPSILON, 'X', __q, "A"));
        transitions.add(new PDATransition(__q, Util.EPSILON, 'A', __p, ""));

        transitions.add(new PDATransition(p, Util.EPSILON, 'A', __p, "A"));
        transitions.add(new PDATransition(p, Util.EPSILON, 'X', __p, "X"));
        transitions.add(new PDATransition(q, Util.EPSILON, 'A', __q, "A"));
        transitions.add(new PDATransition(q, Util.EPSILON, 'X', __q, "X"));

        return new PDA(states, transitions, alphabet, q, stack, 'X');
    }

    private static void checkAnBnPrefix(PDA m) {
        assert AcceptPDA.accept(m, "");
        assert AcceptPDA.accept(m, "ab");
        assert AcceptPDA.accept(m, "aabb");
        assert AcceptPDA.accept(m, "aaabb");
        assert !AcceptPDA.accept(m, "abb");
        assert !AcceptPDA.accept(m, "ba");
        assert !AcceptPDA.accept(m, "aababb");
    }

    private static void runTestA_4() {
        checkAnBnPrefix(getAnBnPrefix());
    }

    private static void runTestA_5() {
        State p = new State("p");
        HashSet<State> states = new HashSet<>();
        states.add(p);

        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('0');
        alphabet.add('1');
        HashSet<Character> stack = new HashSet<>();
        stack.add('1');
        stack.add('F');

        HashSet<PDATransition> transitions = new HashSet<>();
        transitions.add(new PDATransition(p, '0', 'F', p, "F"));
        transitions.add(new PDATransition(p, '1', 'F', p, "1F"));
        transitions.add(new PDATransition(p, '1', '1', p, "11"));
        transitions.add(new PDATransition(p, '0', '1', p, ""));
        transitions.add(new PDATransition(p, Util.EPSILON, 'F', p, ""));

        PDA m = new PDA(states, transitions, alphabet, p, stack, 'F');

        assert AcceptPDA.accept(m, "");
        assert AcceptPDA.accept(m, "10");
        assert !AcceptPDA.accept(m, "01");
        assert !AcceptPDA.accept(m, "110");
        assert AcceptPDA.accept(m, "110000");
        assert AcceptPDA.accept(m, "101010");
        assert !AcceptPDA.accept(m, "000000111");
        assert AcceptPDA.accept(m, "00000000001010");
    }

    private static void runTestA_6() {
        State p = new State("p");
        HashSet<State> states = new HashSet<>();
        states.add(p);

        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('0');
        HashSet<Character> stack = new HashSet<>(alphabet);
        stack.add('F');

        HashSet<PDATransition> transitions = new HashSet<>();
        transitions.add(new PDATransition(p, Util.EPSILON, 'F', p, "0F"));
        transitions.add(new PDATransition(p, Util.EPSILON, '0', p, "00"));
        transitions.add(new PDATransition(p, '0', 'F', p, "F"));

        PDA m = new PDA(states, transitions, alphabet, p, stack, 'F');

        assert !AcceptPDA.accept(m, "");
    }

    private static void runTestsA() {
        System.out.print("Running tests for A...");
        runTestA_1();
        runTestA_2();
        runTestA_3();
        runTestA_4();
        runTestA_5();
        runTestA_6();
        System.out.println("Done");
    }

    /**
     * Experimental tests for B. These work if you use a "createPalindromePDA" function which creates
     * a PDA as in the examples above, also your A needs to work. Yes, this brute forces every number
     * in {0, 1}* up to a length of 15
     */

    private static String reverse(String w) {
        StringBuilder res = new StringBuilder(w.length());
        for (int i = w.length() - 1; i >= 0; i--) {
            res.append(w.charAt(i));
        }
        return res.toString();
    }

    private static void runTestsB(PDA m, String w) {
        if (w.length() > 3) {
            return;
        }

        assert AcceptPDA.accept(m, w) == w.equals(reverse(w));
        String v = w + '0';
        runTestsB(m, v);
        v = w + '1';
        runTestsB(m, v);
    }

    private static void runTestsB(PDA m) {
        runTestsB(m, "");
    }

    private static void runTestsB() {
        System.out.print("Running tests for B...");
        PDA m = Palindrome.createPalindromePDA();
        runTestsB(m);
        System.out.println("Done");
    }

    private static void runTestC_1() {
        check01Prefix(PDAPrefix.pdaPrefix(get01PDA()));
    }

    private static void runTestC_2() {
        checkAnBnPrefix(PDAPrefix.pdaPrefix(getAnBnPDA()));
    }

    private static void runTestsC() {
        System.out.print("Running tests for C...");
        runTestC_1();
        runTestC_2();
        System.out.println("Done");
    }

    private static void runTestE_1() {
        HashSet<Terminal> alphabet = new HashSet<>();
        Terminal a = new Terminal('a');
        alphabet.add(a);
        Terminal b = new Terminal('b');
        alphabet.add(b);
        Terminal c = new Terminal('c');
        alphabet.add(c);

        HashSet<NonTerminal> nonTerminals = new HashSet<>();
        NonTerminal S = new NonTerminal("S");
        nonTerminals.add(S);
        NonTerminal A = new NonTerminal("A");
        nonTerminals.add(A);
        NonTerminal B = new NonTerminal("B");
        nonTerminals.add(B);
        NonTerminal C = new NonTerminal("C");
        nonTerminals.add(C);
        NonTerminal D = new NonTerminal("D");
        nonTerminals.add(D);
        NonTerminal E = new NonTerminal("E");
        nonTerminals.add(E);
        NonTerminal F = new NonTerminal("F");
        nonTerminals.add(F);

        HashSet<Production> productions = new HashSet<>();
        productions.add(new Production(S, A, B));
        productions.add(new Production(A, C, D));
        productions.add(new Production(A, C, F));
        productions.add(new Production(B, c));
        productions.add(new Production(B, E, B));
        productions.add(new Production(C, a));
        productions.add(new Production(D, b));
        productions.add(new Production(E, c));
        productions.add(new Production(F, A, D));

        Grammar g = new Grammar(alphabet, nonTerminals, productions, S);

        assert CYK.cyk(g, "aaabbbcc").toString().equals(
                "{S}\n" +
                        "{S}{}\n" +
                        "{A}{}{}\n" +
                        "{}{F}{}{}\n" +
                        "{}{A}{}{}{}\n" +
                        "{}{}{F}{}{}{}\n" +
                        "{}{}{A}{}{}{}{B}\n" +
                        "{C}{C}{C}{D}{D}{D}{B,E}{B,E}");
    }

    private static void runTestE_2() {
        HashSet<Terminal> alphabet = new HashSet<>();
        Terminal a = new Terminal('a');
        alphabet.add(a);
        Terminal b = new Terminal('b');
        alphabet.add(b);

        HashSet<NonTerminal> nonTerminals = new HashSet<>();
        NonTerminal S = new NonTerminal("S");
        nonTerminals.add(S);
        NonTerminal A = new NonTerminal("A");
        nonTerminals.add(A);
        NonTerminal B = new NonTerminal("B");
        nonTerminals.add(B);
        NonTerminal X = new NonTerminal("X");
        nonTerminals.add(X);
        NonTerminal Y = new NonTerminal("Y");
        nonTerminals.add(Y);
        NonTerminal Z = new NonTerminal("Z");
        nonTerminals.add(Z);

        HashSet<Production> productions = new HashSet<>();
        productions.add(new Production(S, A, X));
        productions.add(new Production(S, B, Y));
        productions.add(new Production(A, a));
        productions.add(new Production(A, A, A));
        productions.add(new Production(B, b));
        productions.add(new Production(B, B, B));
        productions.add(new Production(X, Z, B));
        productions.add(new Production(Y, Z, A));
        productions.add(new Production(Z, A, B));
        productions.add(new Production(Z, B, A));

        Grammar g = new Grammar(alphabet, nonTerminals, productions, S);

        assert CYK.cyk(g, "baabbaa").toString().equals(
                "{S}\n" +
                        "{S}{Y}\n" +
                        "{X}{Y}{Y}\n" +
                        "{X}{S,X,Z}{Y}{S,Y,Z}\n" +
                        "{Y,Z}{Z}{X,Z}{Z}{Y,Z}\n" +
                        "{Z}{A}{Z}{B}{Z}{A}\n" +
                        "{B}{A}{A}{B}{B}{A}{A}");
    }

    private static void runTestE_3() {
        HashSet<Terminal> alphabet = new HashSet<>();
        Terminal a = new Terminal('a');
        alphabet.add(a);
        Terminal b = new Terminal('b');
        alphabet.add(b);
        Terminal c = new Terminal('c');
        alphabet.add(c);

        HashSet<NonTerminal> nonTerminals = new HashSet<>();
        NonTerminal S = new NonTerminal("S");
        nonTerminals.add(S);
        NonTerminal A = new NonTerminal("A");
        nonTerminals.add(A);
        NonTerminal B = new NonTerminal("B");
        nonTerminals.add(B);
        NonTerminal C = new NonTerminal("C");
        nonTerminals.add(C);
        NonTerminal T = new NonTerminal("T");
        nonTerminals.add(T);
        NonTerminal U = new NonTerminal("U");
        nonTerminals.add(U);

        HashSet<Production> productions = new HashSet<>();
        productions.add(new Production(S, T, S));
        productions.add(new Production(S, C, T));
        productions.add(new Production(S, a));
        productions.add(new Production(T, A, U));
        productions.add(new Production(T, T, T));
        productions.add(new Production(T, c));
        productions.add(new Production(U, S, B));
        productions.add(new Production(U, A, B));
        productions.add(new Production(A, a));
        productions.add(new Production(B, b));
        productions.add(new Production(C, c));

        Grammar g = new Grammar(alphabet, nonTerminals, productions, S);

        assert CYK.cyk(g, "ccaab").toString().equals(
                "{S,T}\n" +
                        "{}{S,T}\n" +
                        "{S}{}{T}\n" +
                        "{S,T}{S}{}{U}\n" +
                        "{C,T}{C,T}{A,S}{A,S}{B}"
        );

        assert CYK.cyk(g, "aabcc").toString().equals(
                "{S,T}\n" +
                        "{T}{}\n" +
                        "{T}{}{}\n" +
                        "{}{U}{}{S,T}\n" +
                        "{A,S}{A,S}{B}{C,T}{C,T}"
        );
    }

    private static void runTestsE() {
        System.out.print("Running tests for E...");
        runTestE_1();
        runTestE_2();
        runTestE_3();
        System.out.println("Done");
    }

    public static void main(String[] args) {
        boolean assertOn = false;
        assert assertOn = true;
        if (!assertOn) {
            throw new RuntimeException("Assertions are disabled");
        }
        runTestsA();
        /* !!!READ COMMENT ABOUT B!!! */
        runTestsB();
        runTestsC();
        runTestsE();
    }
}
